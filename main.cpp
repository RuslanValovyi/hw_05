#include <iostream>

using namespace std;
#define countof(array) (sizeof(array)/sizeof(*(array)))

void bubble_sort(int a[], int sz) 
{ 
	for (int i = 0; i < sz-1; i++) {
		for (int j = 0; j < sz-i-1; j++) {
			if (a[j] > a[j+1]) {
				int t  = a[j];
				a[j]   = a[j+1];
				a[j+1] = t;
			}
		}
	}
} 

void printArray(int a[], int sz) 
{ 
    for (int i=0; i < sz; i++) { 
        cout << a[i] << " ";
	} 
    cout << endl; 
}

void reverse_number(long in)
{
	if (in == 0) {
		return;
	}
    cout << in%10 << " ";
	reverse_number(in/10);
} 

int main() 
{ 
	int array[] = {16, 64, 1, 32, 4, 8, 2}; 
	int sz = countof(array); 
	cout << "Unsorted array: "; 
	printArray(array, sz); 
	bubble_sort(array, sz); 
	cout << "Sorted array: "; 
	printArray(array, sz); 
	long number = 1234567890;
	cout << "Original number: " << number << endl; 
	cout << "Reverse number: "; 
	reverse_number(number);
	return 0; 
} 
