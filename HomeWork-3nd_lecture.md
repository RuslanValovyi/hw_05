homework is next:

1. Implement any sorting algorithm you like using cycles. 
   Function should take an array as a parameter (and a size of it) and modify 
   the array without creating a new one.
2. Create a recursive function which prints given number (long) backwards, 
   splitting it with spaces (for example  123456789 --> 9 8 7 6 5 4 3 2 1)
   It is prohibited to use arrays, strings, lists, etc. Only recursion and 
   integer calculations are allowed
